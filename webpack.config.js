const path = require('path')
const NODE_ENV = process.env.NODE_ENV || 'development'
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const glob = require('glob');
const fs = require('fs')
const webpack = require('webpack')
const htmlPages = generateHtmlPlugins('./prod/pages')

let source_map = 'production',
    isWatching = false

if (NODE_ENV === 'development') {
  source_map = 'development'
  isWatching = true
}

// console.log(convertToObject(glob.sync('./prod/pages/*')))
module.exports = {
  context: __dirname + '/prod',
  entry: convertToObject(glob.sync('./prod/pages/*')),
  mode: NODE_ENV,
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js'
  },
  optimization:{
    splitChunks:{
      chunks: 'all'
    }
  },
  // resolve: {
  //   extensions: [
  //     '.js',
  //     '.css',
  //     '.html',
  //     '.pug'
  //   ]
  // },
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    compress: true,
    port: 3000
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              // presets: ["es2015"]
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: {
                      ie: 11,
                    },
                  }
                ]
              ],
              plugins: [
                ['@babel/plugin-transform-runtime']
              ],
            }
          }
        ]
      },
      {
        test: /\.pug$/,
        use: ['pug-loader']
      },
      // {
      //   test: /\.css$/i,
      //   use: [
      //     {
      //       loader: MiniCssExtractPlugin.loader,
      //     },
      //     'css-loader'
      //   ],
      // },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              sourceMap: NODE_ENV === "development" ? true : false,
              minimize: NODE_ENV === "development" ? true : false
            }
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        //type: 'asset/img'
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '/assets/img/',
              //outputPath: '/assets/img',
              name: '[path][name].[ext]',
            }
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.json$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ]
  },
  devtool: source_map,
  watch: isWatching,
  plugins: [
    new MiniCssExtractPlugin({
      filename: isWatching ? 'css/[name].css' : 'css/[name].[hash].css',
      chunkFilename: isWatching ? '[id].css' : '[id].[hash].css',
    }),
    new CleanWebpackPlugin(),
    // new webpack.LoaderOptionsPlugin({
    //   options: {
    //     experiments: {
    //       asset: true
    //     },
    //   }
    // }),
    new CopyPlugin([
      {
        from: path.resolve(__dirname, 'prod/assets'),
        to: path.resolve(__dirname, 'dist/assets')
      },
    ]),
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    //   jQuery: 'jquery'
    // })
  ]
  .concat(htmlPages)
};

function convertToObject(arr){
  return arr.reduce((obj, item, i) => {
    obj[item.replace('./prod/pages/', '')] = `./pages/${arr[i].replace('./prod/pages/', '').replace('.js', '')}/${item.replace('./prod/pages/', '').replace('.js', '')}.js`
    return obj
  }, {})
}

function excludeChunks (obj, name){
   let excludes = []
   for (let itm in obj) {
     if(itm !== name){
       excludes.push(itm)
     }
   }
   return excludes
}

function generateHtmlPlugins (templateDir) {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
  return templateFiles.map((item) => {
    const name = item
    const extension = ['html','pug']
    // console.log(name,templateDir)
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      //template: path.resolve(__dirname, `${templateDir}/${name}/${name}.${extension[0]}`),
      template: path.resolve(__dirname, `${templateDir}/${name}/${name}.${extension[1]}`),
      excludeChunks: excludeChunks(convertToObject(glob.sync('./prod/pages/*')),name),
    })
  })
}
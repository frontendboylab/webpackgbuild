'use strict'
import '../../css-common/common.scss'
import './app.scss'
import { monthConfig } from './month.js';
//const page = {pageName: 'app'}
//const urlTemperature = 'https://jsonplaceholder.typicode.com/posts/1/comments';

const urlPerceptation = '/assets/pt.json';
const urlTemperature = '/assets/tmp.json';

class indexedDbManager{

  constructor() {
    this.name = "db_manager_class"
  }

  dbTransactionName (db,objConfig) {
    const {
      dbStoreName,
      dbIndexName,
      data,
      action,
      method,
    } = objConfig;
    let request;
    let transaction = {}
    let store = {}

    try {
      transaction = db.transaction(dbStoreName, method);
      store = transaction.objectStore(dbStoreName);
    } catch (e) {
      this.errorLoger(e,'transaction-error')
      db.close()
      return
    }

    return new Promise((resolve,reject) => {
      switch (action) {
        case 'add':
          request = store.add(data, dbIndexName);
          request.onerror = function (e){
            resolve({value:e,msg:'error'});
          },
          request.onsuccess = function (e) {
            resolve(request.result);
            db.close();
          };
          break;
        // case 'put':
        //
        //   break;
        case 'delete':
          request = store.delete(dbIndexName);
          //request.onerror =
          request.onsuccess = function (e) {
            resolve(request.result);
            db.close();
          };
          break;
        case 'get':
          request = store.get(dbIndexName);
          request.onerror = function (e) {
            resolve({value:e,msg:'error'});
          },
          request.onsuccess = function (e) {
            resolve(request.result);
            db.close();
          };
          break;
        default:
          break;
      }
    })
   }

  dbTransaction (objConfig) {
    const request = indexedDB.open(objConfig.dbName);
    const _this = this;
    return new Promise((resolve,reject) => {
      request.onsuccess = function (e) {
        const db = e.target.result;
        const version = db.version;
        const versionRequest = indexedDB.open(objConfig.dbName,version);
        versionRequest.onsuccess = function(e) {
          const db = e.target.result;
          resolve(_this.dbTransactionName(db,objConfig));
          request.onerror = function(event) {
            if (request.error.name === "ConstraintError") {
              console.log('USE OTHER STORE KEY NAME');
            }
          }
        }
       };
       request.onerror = function (e) {
         reject(e);
         console.log('ERROR_CONNECT')
      }
    })
  }

  dbVersionControll (db,dbStoreName) {
    let activeStores = Object.values(db.objectStoreNames);
    return dbStoreName.filter((a) => activeStores.indexOf(a) === -1)
  }

  errorLoger (msg,err,key) {
    console.log(msg,err)
  }

  addObjectsStore (db,versionsControll) {
    try {
      for(let i = 0; i<versionsControll.length; i++){
       db.createObjectStore(versionsControll[i]);
      }
     } catch(err) {
       this.errorLoger('error: create objectstore error', err,false)
    }
  }

  connectDbStore (objConfig) {

    const {
      dbName,
      dbStoreName,
      version
    } = objConfig;
    let _this = this;

    return new Promise ((resolve, reject) => {
      let request = indexedDB.open(dbName,version, (db) => {
        //const version = db.version;
        const versionsControll = _this.dbVersionControll(db, dbStoreName);
        _this.addObjectsStore(db, versionsControll)
      });
      request.onsuccess = function (e) {
        const db = e.target.result;
        const version = db.version;
        if(db.objectStoreNames.length === 0){
          indexedDB.deleteDatabase(dbName);
          window.location.reload()
        }
        try {
          resolve({
            version: version,
            db: db,
            status: 'connected'
          });
        } catch (e) {
          console.log('error in connect');
        }
      };
      request.onupgradeneeded = function (e) {
        console.log(e);
        const db = e.target.result;
        const versionsControll = _this.dbVersionControll(db, dbStoreName);
        _this.addObjectsStore(db, versionsControll);
      };
      request.onerror = function (e) {
        reject(e);
        console.log('ERROR_CONNECT',e)
      }
    })
  }
}

class App extends indexedDbManager {
  constructor (name) {
    super();
    const _this = this;
    this.name = name;
    this.data = {
      elements: {
        app:document.getElementById('app')
      },
      dbStorageName:[
        'db_temperature',
        'db_perceptation',
      ],
      defaultLoad:{
        db_name:'db_temperature',
        store_name:'all_temperature',
        fieldName: 'temperature',
        url: urlTemperature
      },
      configSelects: [
        {
          id:1,
          className: 'app--select',
          parentClassName: 'app--select-option',
        },
        {
          id:2,
          className: 'app--select',
          parentClassName: 'app--select-option',
        },
      ],
      configBtnsLoad: [
        {
          id:1,
          name: 'температура',
          fieldName: 'temperature',
          url: urlTemperature,
          className: 'app--button',
          parentClassName: 'app--button-box',
        },
        {
          id:2,
          name: 'осадки',
          fieldName: 'perceptation',
          url: urlPerceptation,
          className: 'app--button',
          parentClassName: 'app--button-box',
        },
      ],
      store: {
        all_temperature: null,
        all_perceptation: null,
        all_datas: [],
        currentDatas: ['1881','2006'],
        currentDataValue: {
          datas: monthConfig.datas,
          type:monthConfig.type,
          _reactiveValue:monthConfig.value,

        },
        currentType: 'temperature'
      }
    }
  }

  async init(){
    let _this = this;
    let defaultload = {};
    Object.defineProperties(this.data.store.currentDataValue, {
      _reactiveValue:{
        enumerable: false,
      },
      value:{
        get () {
          return this._reactiveValue
        },
        set (val) {
          _this.watcher('set',val)
          this._reactiveValue = val
        }
      }
    });

    await this.connectDbStore({
      dbName:'app-db',
      dbStoreName:this.data.dbStorageName,
      version: 1,
    });

    defaultload = await this.dbTransaction({
      dbName:'app-db',
      dbStoreName:_this.data.defaultLoad.db_name,
      dbIndexName:'full-datas',
      data: null,
      action:'get',
      method:'readonly'
    });

    if (defaultload) {
      this.data.store[this.data.defaultLoad.store_name] = defaultload
    } else {
      this.data.store[this.data.defaultLoad.store_name] = await this.getDatas(this.data.defaultLoad.url,this.data.defaultLoad.fieldName);
      await _this.dbTransaction({
        dbName:'app-db',
        dbStoreName:this.data.defaultLoad.db_name,
        dbIndexName:'full-datas',
        data:this.data.store[this.data.defaultLoad.store_name],
        action:'add',
        method:'readwrite'
      })
    }

    this.data.store.all_datas = await calculateClass.calculateYears(this.data.store[this.data.defaultLoad.store_name]);
    this.buildSelects(this.data.store.all_datas);
    this.buildButtons();
    this.setDataValues();
    canvasBuilder.init(this);
    //canvasBuilder.build(this.data.store.currentDataValue);
    console.log(this)
  }

  async watcher () {

    const {
      currentDatas,
      currentType
    } = this.data.store
    //let copy = {}

    this.data.store.currentDataValue.datas = currentDatas
    this.data.store.currentDataValue.type = currentType

    const answer = await this.dbTransaction({
      dbName:'app-db',
      dbStoreName: `db_${currentType}`,
      dbIndexName: currentDatas.join('-'),
      data: {
        type: this.data.store.currentDataValue.type,
        datas: this.data.store.currentDataValue.datas,
        value: this.data.store.currentDataValue.value
      },
      action: 'add',
      method: 'readwrite'
    })
    //copy = Object.assign(this.data.store.currentDataValue,{})
    canvasBuilder.build(this.data.store.currentDataValue)
    console.log('watch',this.data.store.currentDataValue)
  }

  preloaderEvents (status,elm) {
   if(status){
     elm.classList.add('loaded')
   } else {
     elm.classList.remove('loaded')
   }
  }

  buildSelects (data) {
    let block = document.createElement('div');
    let selectBlock = {};
    block.classList.add(this.data.configSelects[0].parentClassName);

    for (let i = 0; i<this.data.configSelects.length; i++) {
      if (i === 1) {
        data.reverse();
      }
      selectBlock = buildSelect(data);
      selectBlock.classList.add(this.data.configSelects[0].className);
      selectBlock.setAttribute('select-id', i);
      selectBlock.onclick = this.setCurrentDatasValue.bind(selectBlock,this);
      block.append(selectBlock);
    }

    this.data.elements[`select_block`] = block;
    this.data.elements.app.append(block);
  }

  buildButtons () {
    let btnS = this.data.configBtnsLoad;
    let _this = this;
    let btnBlock = document.createElement('div');
    btnBlock.classList.add(this.data.configBtnsLoad[0].parentClassName)
    for( let i = 0; i<btnS.length; i++ ){
      let elm = document.createElement('button');
      elm.innerHTML = btnS[i].name;
      elm.classList.add(this.data.configBtnsLoad[i].className)
      elm.setAttribute('value-type',btnS[i].fieldName)
      elm.onclick = function(){
        _this.data.store.currentType = this.getAttribute('value-type')
        _this.preloaderEvents(true,_this.data.elements.app);
        _this.connectDbStore({
          dbName:'app-db',
          dbStoreName:_this.data.dbStorageName,
          version: 1,
        })
          .then((data) => {
            _this.dbTransaction({
              dbName:'app-db',
              dbStoreName:'db_'+ btnS[i].fieldName +'',
              dbIndexName:'full-datas',
              data: null,
              action:'get',
              method:'readonly'
            })
            .then((e) => {
              if(!e){
               return _this.getDatas(btnS[i].url,btnS[i].fieldName)
              } else {
                _this.data.store[`all_${btnS[i].fieldName}`] = e;
              }
            })
            .then((data) => {
              if(data){
                _this.data.store[`all_${btnS[i].fieldName}`] = data;
                _this.dbTransaction({
                  dbName: 'app-db',
                  dbStoreName: 'db_' + btnS[i].fieldName + '',
                  dbIndexName: 'full-datas',
                  data: data,
                  action: 'add',
                  method: 'readwrite'
                })
                  .then(() => {
                    _this.getDataValues().then(() => { _this.preloaderEvents(false, _this.data.elements.app) });
                  })
              } else {
                  _this.getDataValues().then(() => { _this.preloaderEvents(false, _this.data.elements.app) });
              }
            })
        })
      }
      btnBlock.append(elm)
    }
    this.data.elements[`btn_box`] = btnBlock;
    this.data.elements.app.append(btnBlock);
  }

  notify (key,msg) {
    switch (key) {
      case 'error':
        //console.log(msg)
        break;
      default:
        break;
    }
  }

  setCurrentDatasValue (_this) {
    _this.data.store.currentDatas[this.getAttribute('select-id')] = this.value;
    _this.getDataValues();
  }

  async getDataValues () {
    const currentType = this.data.store.currentType;
    const currentDatas = this.data.store.currentDatas.join('-');

    const answer = await this.dbTransaction({
      dbName: 'app-db',
      dbStoreName: 'db_' + currentType + '',
      dbIndexName: currentDatas,
      data: null,
      action: 'get',
      method: 'readonly'
    })

    if(answer){
      setResult(this,answer.value)
    } else {
      this.setDataValues()
    }
  }

  async setDataValues () {

    const data = this.data.store[`all_${this.data.store.currentType}`]
    const result = await calculateClass.calculateCurrentDatasValue(this.data.store.currentDatas,data)
    setResult(this,result)

  }

  eventsManager (event,func){
    switch (event) {
      case 'click':
          //console.log('click')
          func();
        break;
    }
  }

  getDatas(url,fieldName){
    // console.log('start-fetching')
    return fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        return data
      })
  }
}

class calculateClass extends App {
  constructor() {
    super();
    this.name = 'calculate_class'
  }
  static calculateYears(data){
    console.log('calculate-years')
    let result = []
    return new Promise((resolve,reject)=>{
      setTimeout(() => {
        data.forEach((e) => { result.push(e.t.split('-')[0]) });
        resolve(result.filter((e,i,arr) => {
          return e !== arr[i+1]
        }))
      },0)
    })
  }
  static calculateCurrentDatasValue(values,data) {
    console.log('calculate-data')
    let start = parseInt(values[0]);
    let end = parseInt(values[1]);
    let count = 0;
    let objDatas = monthConfig.value
    clearMonthValues(objDatas)

    if(start <= end) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          for (let i = 0; i <= data.length; i++) {
            if (data[i]) {
              if (parseInt(data[i].t.split('-')[0]) < start || parseInt(data[i].t.split('-')[0]) > end) {
                continue;
              }
              count++;
              for (let key in objDatas) {
                if (parseInt((data[i].t.split('-')[1])) === parseInt(key)) {
                  objDatas[key].value += parseInt(data[i].v)
                  objDatas[key].days = objDatas[key].days + 1
                }
              }
            }
          }
          resolve(objDatas)
        }, 0)
      })
    } else {
      return false
    }
  }
}

class CanvasBuilder  {
  constructor () {
    this.name = 'canvas_builder_class';
  }
  init (app) {
    let canvasBox = document.createElement('div')
    this.canvas = document.createElement('canvas')
    this.app = app;
    this.ctx = this.canvas.getContext('2d')

    this.canvas.classList.add('app--canvas');
    canvasBox.classList.add('app--canvas-box');
    canvasBox.append(this.canvas);
    app.data.elements.app.append(canvasBox);
    app.data.elements[`app_canvas`] = canvasBox;

    this.canvas.width = 500;
    this.canvas.height = 500;
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

  }
  build (data) {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.type = data.type

    let x,
        y;
    let width = 3;
    let heightNum = 10;

    Object.keys(data.value).forEach((key) => {
      x = ((this.canvas.width / 13) * key) - width,
      y = (this.canvas.height / 2) + heightNum;
      this.buildLIne({
        val: data.value[key],
        key: key,
        lineW: width,
        lineS: heightNum,
        x: x,
        y: y,
        gradient: [
          '#1c6192',
          '#c36363',
          '#d02222',
        ],
      })
      console.log(data.value[key].name)
      this.setText(key,'bolder 9px Montserrat',x,20)


      this.ctx.beginPath();
      this.ctx.rotate(20 * Math.PI / 180);
      this.setText(data.value[key].name,'bolder 9px Montserrat',x,this.canvas.height - 20)

      this.ctx.closePath()

    })
    //_this
  }
  buildLIne (obj) {

    let {
      val,
      key,
      x,
      y,
      lineW,
      lineS,
      gradient
    } = obj;

    let grd = {};
    let textX = 0;
    let textY = 0;

    const padding = 15;
    // let x = ((this.canvas.width / 13) * key) - lineW,
    //     y = (this.canvas.height / 2) + padding;

    if (Math.sign(val.average) === 1 && this.type === 'temperature') {
      grd = this.ctx.createLinearGradient(x, val.average * lineS, x + (x / 2), y + (y / 2))
      grd.addColorStop(1, gradient[1]);
      grd.addColorStop(0.2, gradient[1]);
      grd.addColorStop(0, gradient[2]);
      textX = x
      textY = (y - val.average * lineS) - padding

    } else if (Math.sign(val.average) !== 1 && this.type === 'temperature') {
      grd = this.ctx.createLinearGradient(x + (lineW/2), y , x + (lineW/2), y + (y / 2))
      grd.addColorStop(1, gradient[0]);
      grd.addColorStop(0, gradient[1]);
      textX = x
      textY = (y - val.average * lineS) + padding

    } else if (Math.sign(val.average) === 1 && this.type === 'perceptation'){
      grd = gradient[0]
      lineS = lineS *3
      textX = x
      textY = (y - val.average * lineS) - padding
    }

    this.ctx.fillStyle = grd;
    this.ctx.fillRect(x, y, lineW, -(val.average * lineS));
    this.setText(val.average,'bolder 11px Montserrat',textX,textY)

  }
  buildCircle () {

  }
  setText (str,fontSyle,x,y) {
    if(this.type === 'temperature' && !isNaN(str)){
      str = parseInt(str)
    }
    this.ctx.font = fontSyle
    this.ctx.fillText(str, x,y)
    this.ctx.fillStyle = '#000'
  }
}

const buildSelect = (arrDatas) => {
  let select =  document.createElement("select");
  let option = {};

  for(let i = 0; i<arrDatas.length; i++){
    option = document.createElement("option");
    option.value = arrDatas[i]
    option.innerHTML = arrDatas[i]
    select.append(option)
  }
  return select
}

const clearMonthValues = (data) => {
  for(let key in data){
    data[key].value = 0
    data[key].days = 0
  }
}

const setResult = (_this,result) => {
  if (result) {
    for(let key in result){
      result[key].average = parseFloat(result[key].value / result[key].days).toFixed(2)
    }
    _this.data.store.currentDataValue.value = result;
  } else {
    _this.notify('error','Первая дата не может быть больше второй!');
  }
}

const app = new App('js-app');
const canvasBuilder = new CanvasBuilder();

app.init();


//module.exports = page
